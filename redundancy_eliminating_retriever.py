from langchain.embeddings.base import Embeddings
from langchain_community.vectorstores import Chroma
from langchain.schema import BaseRetriever


class RedundancyEliminatingitriever(BaseRetriever):
    embeddings: Embeddings  # Class attribute embeddings of type Embeddings
    chroma: Chroma  # Class attribute chroma of type Chroma

    def get_relevant_documents(self, query):
        emb = self.embeddings.embed_query(query)

        return self.chroma.max_marginal_relevance_search_by_vector(
            embedding=emb,
            lambda_mult=0.8  # Determines the degree of diversity among the results with 0 corresponding to maximum diversity
        )
