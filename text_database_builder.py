from langchain_community.document_loaders import TextLoader
from langchain.text_splitter import CharacterTextSplitter
from langchain_openai import OpenAIEmbeddings
from langchain.vectorstores.chroma import Chroma
from dotenv import load_dotenv

load_dotenv()

# Load the document and split it into chunks
loader = TextLoader("trivia.txt")

text_splitter = CharacterTextSplitter(
    separator="\n",
    chunk_size=200,
    chunk_overlap=0
)

docs = loader.load_and_split(
    text_splitter=text_splitter
)

# Initialize OpenAI embeddings
embeddings = OpenAIEmbeddings()

# Save to disk
db = Chroma.from_documents(
    docs,
    embedding=embeddings,
    persist_directory="emb"
)

# Query it
# results = db.similarity_search_with_score("What is an interesting fact about the pangolin?")

# Uncomment the block below to see the top 4 results similar to the query.
# You will notice duplicates in outputs upon running the code above for the second time.
# for result in results:
#     print(result[0].page_content, "\n")
