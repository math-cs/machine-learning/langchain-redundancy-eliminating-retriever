from dotenv import load_dotenv
from langchain_community.vectorstores import Chroma
from langchain_openai import OpenAIEmbeddings
from langchain.chains import RetrievalQA
from langchain_openai import ChatOpenAI
from redundancy_eliminating_retriever import RedundancyEliminatingRitriever

# Uncomment for debug mode
# import langchain
# langchain.debug = True

# Load environment variable OPENAI_API_KEY from .env
load_dotenv()

# Initialize OpenAI embeddings
embeddings = OpenAIEmbeddings()

# Initialize Chroma database with specified persist directory and embedding function
db = Chroma(
    persist_directory="emb",
    embedding_function=embeddings
)

# Uncomment to use the default retriever
# retriever = db.as_retriever()

# We will use our custom retriever rather than Chroma's to filter redundancies
retriever = RedundancyEliminatingRitriever(
    embeddings=embeddings,
    chroma=db
)

# Initialize ChatOpenAI language model
chat = ChatOpenAI()

chain = RetrievalQA.from_chain_type(
    llm=chat,
    retriever=retriever,
    # This chain type takes a list of documents, inserts them /all/ into a prompt and passes that prompt to LLM.
    chain_type="stuff"
)

result = chain.invoke(
    "What is an interesting fact about the pangolin?")["result"]
print(result)
